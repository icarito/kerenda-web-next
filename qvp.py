#!/bin/env python3

from quart import Quart, websocket, abort, send_from_directory
import asyncio
import glob
import os, sys
import sh
import vbuild
import re

app = Quart(__name__)

print (
"""
  --=--
 - qvp -
  --=--
""")

# Mimic Nuxt API

template = """
<!doctype html>
<html>
<head>
<title>K'erenda Homet Nature Reserve</title>
<link rel="icon" type="image/png" href="icon.png" />
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5, minimal-ui" />
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre.min.css">
<link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-exp.min.css">
<link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-icons.min.css">
<!-- head -->
<style>
/* style */
</style>
</head>
<body>
<div id="qvp_app">
<!-- layout -->
<!-- html -->
</div>
<script>
/* script */
new Vue({el:"#qvp_app"})
</script>
</body>
</html>
"""

render_paths = ['components/*.vue']

@app.route('/')
@app.route('/<string:page>')
async def serve(page='index'):
    page_file = os.path.join('pages', page + '.vue')
    page_name = "pages-" + page
    if os.path.isfile(page_file):
        components = vbuild.render(page_file, render_paths)
        head = ""
        html = components.html
        style = components.style
        script = components.script.replace("Vue.component('" + page, 
                                           "Vue.component('" + page_name)

        # Infer layout
        layout_name = "default"
        layout_file = os.path.join('layouts', layout_name + '.vue')
        try:
            for item in re.findall('layout:?=? .*[\'"](.*)[\'"]', script):
                layout_candidate = os.path.join('layouts', item + '.vue')
                if os.path.isfile(layout_candidate):
                    layout_name = item
                    layout_file = layout_candidate
        finally:
            layout_target = 'layouts-' + layout_name
            if os.path.isfile(layout_file):
                layout = vbuild.render(layout_file)

                # also, "default" is a reserved word in javascript
                layout_script = layout.script.replace("Vue.component('" + layout_name, 
                                                      "Vue.component('" + layout_target)\
                                             .replace("default = Vue.component(",
                                                      "layout_default = Vue.component(")\
                                             .replace("var default=(function()",
                                                      "var layout_default=(function()")

                script = script + "\n" + layout_script
                for item in re.findall('head:?=? .*[\'"](.*)[\'"]', script):
                    head += item

                html = html + "\n" + layout.html.replace("<qvp_page/>",  "<" + page_name + ">" \
                                                                      + "</" + page_name + ">")
                style = layout.style + "\n" + style
            else:
                html = html + "\n" + "<" + page_name + "></" + page_name + ">"

        return template.replace('<!-- html -->', html)\
                           .replace('<!-- head -->', head)\
                           .replace('/* script */', script)\
                           .replace('/* style */', style)\
                           .replace('<!-- layout -->', "<" + layout_target + ">" \
                                                    + "</" + layout_target + ">")
    if os.path.exists(os.path.join('static', page)):
        return await send_from_directory('static', page)
    return abort(404)



# Mimic Guy API

@app.websocket('/ws')
async def ws():
    await websocket.send('hello')

async def freeze():
    os.makedirs('dist', exist_ok=True)
    for item in glob.glob('pages/*.vue'):
        route = os.path.splitext(os.path.basename(item))[0]
        new_path = os.path.join('dist', route + '.html')
        with open(new_path, 'w') as outfile:
            print ('Writing', new_path, '...')
            outfile.write(await serve(page=route))
    print ('Copying static files to dist/ ...')
    sh.cp('-rf', *glob.glob('static/*'), 'dist/')

if __name__ == "__main__":
    if len(sys.argv) > 1:
        if sys.argv[1]=='build':
            asyncio.run(freeze())
        else:
            print ('Option not understood')
    else:
        app.run(host="0.0.0.0", port="9520")
